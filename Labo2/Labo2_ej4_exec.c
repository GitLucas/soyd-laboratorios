#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

void spawn(char *command, char *args[])
{
	pid_t pid = fork();

	if(pid<0)
	{
		fprintf(stderr, "Fork fail.");
		exit(1);
	}

	if(pid==0) execvp(command, args);
	else wait(NULL);
}

int main()
{
	char *arg_list[3];
	arg_list[0] = "tree";
	arg_list[1] = "/etc";
	arg_list[2] = NULL;
	spawn("tree", arg_list);
	exit(EXIT_SUCCESS);
}

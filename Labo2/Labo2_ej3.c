#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>

long read_msg;
int fd[2];

void collatz(long i)
{
	while(i>1)
	{
		write(fd[1], &i, sizeof(long));
		if(i%2==0) i /= 2;
		else i = (3*i) + 1;
	}
	write(fd[1], &i, sizeof(long));
}

int main(void)
{
	char line[256], *endptr;
	while(1)
	{
		printf("Ingrese un número entero positivo ['quit' para salir]: ");
		scanf("%s", line);
		if(strcmp(line,"quit") == 0)
		{ printf("Saliendo...\n"); break; }

		if(pipe(fd)<0)
		{
			printf("Pipe failed");
			return 1;
		}
		pid_t pid = fork();

		if(pid==0)
		{
			close(fd[0]);
			collatz(strtol(line, &endptr, 10));
			close(fd[1]);
			exit(EXIT_SUCCESS);
		}
		else
		{
			close(fd[1]);
			wait(NULL);
			while (read(fd[0], &read_msg, sizeof(long)) > 0)
				printf("%d  ", read_msg);
			close(fd[0]);
		}
	}
	return EXIT_SUCCESS;
}

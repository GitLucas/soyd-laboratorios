#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>

void collatz(long i)
{
	while(i>1)
	{
		printf("%d  ", i);
		if(i%2==0) i /= 2;
		else i = (3*i) + 1;
	}
	printf("%d\n", i);
}

int main(void)
{
	char line[256], *endptr;
	while(1)
	{
		printf("Ingrese un número entero positivo ['quit' para salir]: ");
		scanf("%s", line);
		if(strcmp(line,"quit") == 0)
		{ printf("Saliendo...\n"); break; }
		pid_t pid = fork();
		if(pid==0)
		{
			collatz(strtol(line, &endptr, 10));
			return EXIT_SUCCESS;
		}
		else { wait(NULL); }
	}
	return EXIT_SUCCESS;
}

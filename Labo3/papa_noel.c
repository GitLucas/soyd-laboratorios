#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <semaphore.h>

#define WHITE "\033[m"
#define RED   "\033[31m"
#define GREEN "\033[32m"

const int CANT_RENOS = 9;
const int CANT_ELFOS = 10;

int elfos;
int renos;
sem_t papa_sem;
sem_t reno_sem;
sem_t elfo_mutex;
sem_t mutex;

void preparar_trineo()
{
	printf(RED"Preparando el trineo...\n"WHITE);
	sleep(1);
}

void ayudar_elfos()
{
	printf(RED"Ayudando a los elfos...\n"WHITE);
	sleep(1);
}

void *papa_noel()
{
	while(1)
	{
		sem_wait(&papa_sem);
		sem_wait(&mutex);

		if(renos >= CANT_RENOS)
		{
			preparar_trineo();
			for(int r = 0; r < CANT_RENOS; r++)
				sem_post(&reno_sem);
			renos -= CANT_RENOS;
		}
		else if (elfos == 3)
		{
			ayudar_elfos();
		}

		sem_post(&mutex);
	}
	return 0;
}

void obtener_rienda()
{
	printf(GREEN"Reno obteniendo rienda...\n"WHITE);
	sleep(1);
}

void *reno()
{
	while(1)
	{
		sleep(2 + random() % 5);
		sem_wait(&mutex);
		renos += 1;
		if(renos == 9)
			sem_post(&papa_sem);
		sem_post(&mutex);
		sem_wait(&reno_sem);
		obtener_rienda();
	}
	return 0;
}

void obtener_ayuda()
{
	printf(WHITE"Elfo obteniendo ayuda...\n"WHITE);
	sleep(1);
}

void *elfo()
{
	while(1)
	{
		sleep(2 + random() % 5);
		sem_wait(&elfo_mutex);
		sem_wait(&mutex);

		elfos += 1;
		if (elfos == 3)
			sem_post(&papa_sem);
		else
			sem_post(&elfo_mutex);

		sem_post(&mutex);
		sem_post(&elfo_mutex);

		obtener_ayuda();

		sem_wait(&mutex);
		elfos -= 1;
		if (elfos == 0)
			sem_post(&elfo_mutex);
		sem_post(&mutex);
	}
	return 0;
}

int main()
{
	elfos = 0;
	renos = 0;
	sem_init(&papa_sem, 0, 0);
	sem_init(&reno_sem, 0, 0);
	sem_init(&elfo_mutex, 0, 1);
	sem_init(&mutex, 0, 1);

	pthread_t papa_tid;
	pthread_t renos_array[CANT_RENOS];
	pthread_t elfos_array[CANT_ELFOS];
	if(pthread_create(&papa_tid, 0, papa_noel, NULL) < 0)
	{
		printf("ERROR CREANDO LOS HILOS\n");
		exit(1);
	}

	for(int r = 0; r < CANT_RENOS; r++)
		if (pthread_create(&renos_array[r], NULL, reno, NULL) < 0)
		{
			printf("ERROR CREANDO LOS HILOS\n");
			exit(1);
		}

	for(int e = 0; e < CANT_ELFOS; e++)
		if (pthread_create(&elfos_array[e], NULL, elfo, NULL) < 0)
		{
			printf("ERROR CREANDO LOS HILOS\n");
			exit(1);
		}

	pthread_join(papa_tid, NULL);

	sem_destroy(&papa_sem);
	sem_destroy(&reno_sem);
	sem_destroy(&elfo_mutex);
	sem_destroy(&mutex);

	return 0;
}

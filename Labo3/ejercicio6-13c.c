#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <semaphore.h>

sem_t a, b, c;

void *P1(void *ptr)
{
	while(1)
	{
		printf("Thread 1...\n");
		sem_post(&a);
		sem_wait(&b);
	}
	return 0;
}

void *P2(void *ptr)
{
	while(1)
	{
		sem_wait(&c);
		printf("Thread 2...\n");
		sem_post(&b);
	}
	return 0;
}

void *P3(void *ptr)
{
	while(1)
	{
		sem_wait(&a);
		printf("Thread 3...\n");
		sem_post(&c);
	}
	return 0;
}

int main()
{
	sem_init(&a, 0, 0);
	sem_init(&b, 0, 0);
	sem_init(&c, 0, 0);

	pthread_t p1_tid, p2_tid, p3_tid;

	if ( pthread_create(&p1_tid, NULL, P1, NULL) < 0 ||
	     pthread_create(&p2_tid, NULL, P2, NULL) < 0 ||
	     pthread_create(&p3_tid, NULL, P3, NULL) < 0)
	{
		printf("ERROR CREANDO LOS HILOS\n");
		exit(1);
	}

	pthread_join(p1_tid, NULL);
	pthread_join(p2_tid, NULL);
	pthread_join(p3_tid, NULL);

	printf("\n");
	fflush(stdout);

	sem_destroy(&a);
	sem_destroy(&b);
	sem_destroy(&c);

	return 0;
}

#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <semaphore.h>

sem_t a, b;

void *P1(void *ptr)
{
	printf("i1 ");
	sem_post(&a);
	sem_wait(&b);
	printf("i2 ");
	return 0;
}

void *P2(void *ptr)
{
	printf("j1 ");
	sem_post(&b);
	sem_wait(&a);
	printf("j2 ");
	return 0;
}

int main()
{
	sem_init(&a, 0, 0);
	sem_init(&b, 0, 0);

	pthread_t p1_tid, p2_tid;

	if ( pthread_create(&p1_tid, NULL, P1, NULL) < 0 ||
	     pthread_create(&p2_tid, NULL, P2, NULL))
	{
		printf("ERROR CREANDO LOS HILOS\n");
		exit(1);
	}

	pthread_join(p1_tid, NULL);
	pthread_join(p2_tid, NULL);

	printf("\n");
	fflush(stdout);

	sem_destroy(&a);
	sem_destroy(&b);

	return 0;
}

#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <semaphore.h>

sem_t mutex;

void *P1(void *ptr)
{
	sem_wait(&mutex);
	printf("printing... node (1)\n");
	sem_post(&mutex);
	return 0;
}

void *P2(void *ptr)
{
	sem_wait(&mutex);
	printf("printing... node (2)\n");
	sem_post(&mutex);
	return 0;
}

void *P3(void *ptr)
{
	sem_wait(&mutex);
	printf("printing... node (3)\n");
	sem_post(&mutex);
	return 0;
}

int main()
{
	sem_init(&mutex, 0, 1);

	pthread_t p1_tid, p2_tid, p3_tid;

	if ( pthread_create(&p1_tid, NULL, P1, NULL) < 0 ||
	     pthread_create(&p2_tid, NULL, P2, NULL) < 0 ||
	     pthread_create(&p3_tid, NULL, P3, NULL) < 0 )
	{
		printf("ERROR CREANDO LOS HILOS\n");
		exit(1);
	}

	pthread_join(p1_tid, NULL);
	pthread_join(p2_tid, NULL);
	pthread_join(p3_tid, NULL);

	printf("\n");
	fflush(stdout);

	sem_destroy(&mutex);

	return 0;
}

#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <semaphore.h>

sem_t a, b, c;
sem_t mutex;
int c_count;

void *P1(void *ptr)
{
	while(1)
	{
		sem_wait(&b);
		printf("1");
		fflush(stdout);
		sleep(1);
		sem_post(&a);
	}
	return 0;
}

void *P2(void *ptr)
{
	while(1)
	{
		sem_wait(&a);
		printf("2");
		fflush(stdout);
		sleep(1);
		sem_wait(&mutex);
		c_count += 1;
		sem_post(&mutex);
		sem_post(&b);
		sem_post(&c);
	}

	return 0;
}

void *P3(void *ptr)
{
	while(1)
	{
		sem_wait(&c);
		if(c_count > 1)
		{
			sem_wait(&mutex);
			printf("3");
			fflush(stdout);
			c_count -= 1;
			sem_post(&mutex);
		}
	}
	return 0;
}

int main()
{
	sem_init(&a, 0, 0);
	sem_init(&b, 0, 1);
	sem_init(&c, 0, 0);
	sem_init(&mutex, 0, 1);

	pthread_t p1_tid, p2_tid, p3_tid;

	if ( pthread_create(&p1_tid, NULL, P1, NULL) < 0 ||
	     pthread_create(&p2_tid, NULL, P2, NULL) < 0 ||
	     pthread_create(&p3_tid, NULL, P3, NULL) < 0 )
	{
		printf("\033[31m ERROR CREANDO HILOS\033[m");
	}

	pthread_join(p1_tid, NULL);
	pthread_join(p2_tid, NULL);
	pthread_join(p3_tid, NULL);

	sem_destroy(&a);
	sem_destroy(&b);
	sem_destroy(&c);
	sem_destroy(&mutex);

	return 0;
}

#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <semaphore.h>

sem_t s1_2, s1_3, s1_4, s1_5,
	s2_6, s3_7,
	s6_8, s7_8, s4_8, s5_8,
	s8_1;

void print_node(int n)
{
	printf("Yo... Soy... El nodo %d\n", n);
	sleep(1);
}

void *P1(void *ptr)
{
	while(1)
	{
		print_node(1);
		sem_post(&s1_2);
		sem_post(&s1_3);
		sem_post(&s1_4);
		sem_post(&s1_5);
		sem_wait(&s8_1);
	}
	return 0;
}

void *P2(void *ptr)
{
	while(1)
	{
		sem_wait(&s1_2);
		print_node(2);
		sem_post(&s2_6);
	}
	return 0;
}

void *P3(void *ptr)
{
	while(1)
	{
		sem_wait(&s1_3);
		print_node(3);
		sem_post(&s3_7);
	}
	return 0;
}

void *P4(void *ptr)
{
	while(1)
	{
		sem_wait(&s1_4);
		print_node(4);
		sem_post(&s4_8);
	}
	return 0;
}

void *P5(void *ptr)
{
	while(1)
	{
		sem_wait(&s1_5);
		print_node(5);
		sem_post(&s5_8);
	}
	return 0;
}

void *P6(void *ptr)
{
	while(1)
	{
		sem_wait(&s2_6);
		print_node(6);
		sem_post(&s6_8);
	}
	return 0;
}

void *P7(void *ptr)
{
	while(1)
	{
		sem_wait(&s3_7);
		print_node(7);
		sem_post(&s7_8);
	}
	return 0;
}

void *P8(void *ptr)
{
	while(1)
	{
		sem_wait(&s4_8);
		sem_wait(&s5_8);
		sem_wait(&s6_8);
		sem_wait(&s7_8);
		print_node(8);
		sem_post(&s8_1);
	}
	return 0;
}

void init_sem()
{
	sem_init(&s1_2, 0, 0);
	sem_init(&s1_3, 0, 0);
	sem_init(&s1_4, 0, 0);
	sem_init(&s1_5, 0, 0);

	sem_init(&s2_6, 0, 0);
	sem_init(&s3_7, 0, 0);

	sem_init(&s4_8, 0, 0);
	sem_init(&s5_8, 0, 0);
	sem_init(&s6_8, 0, 0);
	sem_init(&s7_8, 0, 0);

	sem_init(&s8_1, 0, 0);
}


void destroy_sem()
{
	sem_destroy(&s1_2);
	sem_destroy(&s1_3);
	sem_destroy(&s1_4);
	sem_destroy(&s1_5);

	sem_destroy(&s2_6);
	sem_destroy(&s3_7);

	sem_destroy(&s4_8);
	sem_destroy(&s5_8);
	sem_destroy(&s6_8);
	sem_destroy(&s7_8);

	sem_destroy(&s8_1);
}

int main()
{
	init_sem();
	pthread_t nodes_t[8];
	void* f[8] = {P1, P2, P3, P4, P5, P6, P7, P8};

	for(int i = 0; i < 8; i++)
		if(pthread_create(&nodes_t[i], NULL, f[i], NULL) < 0)
		{
			printf("ERROR CREANDO LOS HILOS\n");
			exit(1);
		}

	for(int i = 0; i < 8; i++) pthread_join(nodes_t[i], NULL);
	destroy_sem();

	return 0;
}

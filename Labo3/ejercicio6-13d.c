#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <semaphore.h>

sem_t a, b, c, d;

void *P1(void *ptr)
{
	printf("Thread 1...\n");
	sleep(1);
	sem_post(&a);
	sem_post(&b);
	return 0;
}

void *P2(void *ptr)
{
	sem_wait(&a);
	printf("Thread 2...\n");
	sleep(1);
	sem_post(&c);
	return 0;
}

void *P3(void *ptr)
{
	sem_wait(&b);
	printf("Thread 3...\n");
	sleep(1);
	sem_post(&d);
	return 0;
}

void *P4(void *ptr)
{
	sem_wait(&c);
	sem_wait(&d);
	printf("Thread 4...\n");
	sleep(1);
	return 0;
}

int main()
{
	sem_init(&a, 0, 0);
	sem_init(&b, 0, 0);
	sem_init(&c, 0, 0);
	sem_init(&d, 0, 0);

	pthread_t p1_tid, p2_tid, p3_tid, p4_tid;

	if ( pthread_create(&p1_tid, NULL, P1, NULL) < 0 ||
	     pthread_create(&p2_tid, NULL, P2, NULL) < 0 ||
	     pthread_create(&p3_tid, NULL, P3, NULL) < 0 ||
	     pthread_create(&p4_tid, NULL, P4, NULL) < 0 )
	{
		printf("ERROR CREANDO LOS HILOS");
		exit(1);
	}

	pthread_join(p1_tid, NULL);
	pthread_join(p2_tid, NULL);
	pthread_join(p3_tid, NULL);
	pthread_join(p4_tid, NULL);

	printf("\n");
	fflush(stdout);

	sem_destroy(&a);
	sem_destroy(&b);
	sem_destroy(&c);
	sem_destroy(&d);

	return 0;
}

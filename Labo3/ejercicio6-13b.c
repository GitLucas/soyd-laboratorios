#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <semaphore.h>

sem_t a, b;

void *P1(void *ptr)
{
	printf("Y ");
	sem_post(&a);
	return 0;
}

void *P2(void *ptr)
{
	sem_wait(&a);
	sem_wait(&b);
	printf("X ");
	return 0;
}

void *P3(void *ptr)
{
	printf("Z ");
	sem_post(&b);
	return 0;
}

int main()
{
	sem_init(&a, 0, 0);
	sem_init(&b, 0, 0);
	pthread_t p1_tid, p2_tid, p3_tid;

	if ( pthread_create(&p1_tid, NULL, P1, NULL) < 0 ||
	     pthread_create(&p2_tid, NULL, P2, NULL) < 0 ||
	     pthread_create(&p3_tid, NULL, P3, NULL) < 0)
	{
		printf("ERROR CREANDO LOS HILOS\n");
		exit(1);
	}

	pthread_join(p1_tid, NULL);
	pthread_join(p2_tid, NULL);
	pthread_join(p3_tid, NULL);

	printf("\n");
	fflush(stdout);

	sem_destroy(&a);
	sem_destroy(&b);
	return 0;
}
